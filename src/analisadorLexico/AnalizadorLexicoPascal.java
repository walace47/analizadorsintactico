package analisadorLexico;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AnalizadorLexicoPascal {

    private LectorArchivoFuente lector;
    private static String cadena;
    private LinkedList<Token> listaTokens;

    public static CreadorTokens creador;

    public AnalizadorLexicoPascal(String archivo) {
	listaTokens = new LinkedList<Token>();
	creador = new CreadorTokens();
	try {
	    lector = new LectorArchivoFuente(archivo);
	} catch (FileNotFoundException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    public String automata() {
	//La cadena analizar y por donde comienza el analisis
	String cadenaRespuesta = "";
	char puntero;
	String valorToken;
	Token token;
	int longitud = cadena.length();
	int longToken;
	if (!cadena.equals("")) {
	    puntero = cadena.charAt(0);
	    //Verifico si la primer palabra es letra, considerando letra como lo definido por nuestro alfabeto
	    if (esLetra(puntero)) {
		cadenaRespuesta = sospechaId() + " " + automata();
		//Verifico si la primer letra es un digito
	    } else if (Character.isDigit(puntero)) {
		cadenaRespuesta = sospechaNumero() + " " + automata();
	    } else {
		switch (puntero) {
		    //salteo espacios en blanco
		    case ' ':
			cadena = cadena.substring(1);
			cadenaRespuesta = automata();
			break;
		    case '	'://Tabs
			cadena = cadena.substring(1);
			cadenaRespuesta = automata();
			break;
		    //se consume toda la cadena hasta el cierre del comentario    
		    case '{':
			cadenaRespuesta = consumirComentario() + automata();
			break;
		    //copio salto de linea    
		    case '\n':
			cadena = cadena.substring(1);
			cadenaRespuesta = "\n" + automata();
			break;
		    //token_suma	
		    case '+':
			valorToken = cadena.substring(0, 1);
			cadena = cadena.substring(1);
			token = creador.getToken(valorToken);
			listaTokens.add(token);
			cadenaRespuesta = token + " " + automata();
			break;
		    //token_Resta	
		    case '-':
			valorToken = cadena.substring(0, 1);
			cadena = cadena.substring(1);
			token = creador.getToken(valorToken);
			listaTokens.add(token);
			cadenaRespuesta = token + " " + automata();
			break;
		    //Token_multiplicacion	
		    case '*':
			valorToken = cadena.substring(0, 1);
			cadena = cadena.substring(1);
			token = creador.getToken(valorToken);
			listaTokens.add(token);
			cadenaRespuesta = token + " " + automata();
			break;
		    //Token_Division	
		    case '/':
			valorToken = cadena.substring(0, 1);
			cadena = cadena.substring(1);
			token = creador.getToken(valorToken);
			listaTokens.add(token);
			cadenaRespuesta = token + " " + automata();
			break;
		    //Token_parentesis que abre	
		    case '(':
			valorToken = cadena.substring(0, 1);
			cadena = cadena.substring(1);
			token = creador.getToken(valorToken);
			listaTokens.add(token);
			cadenaRespuesta = token + " " + automata();
			break;
		    //token_parentesis_que_cierra	
		    case ')':
			valorToken = cadena.substring(0, 1);
			cadena = cadena.substring(1);
			token = creador.getToken(valorToken);
			listaTokens.add(token);
			cadenaRespuesta = token + " " + automata();
			break;
		    //token igual	
		    case '=':
			valorToken = cadena.substring(0, 1);
			cadena = cadena.substring(1);
			token = creador.getToken(valorToken);
			listaTokens.add(token);
			cadenaRespuesta = token + " " + automata();
			break;
		    //token_coma	
		    case ',':
			valorToken = cadena.substring(0, 1);
			cadena = cadena.substring(1);
			token = creador.getToken(valorToken);
			listaTokens.add(token);
			cadenaRespuesta = token + " " + automata();
			break;
		    //token_fin_sentencia	
		    case ';':
			valorToken = cadena.substring(0, 1);
			cadena = cadena.substring(1);
			token = creador.getToken(valorToken);
			listaTokens.add(token);
			cadenaRespuesta = token + " " + automata();
			break;
		    //token_fin_programa	
		    case '.':
			valorToken = cadena.substring(0, 1);
			cadena = cadena.substring(1);
			token = creador.getToken(valorToken);
			listaTokens.add(token);
			cadenaRespuesta = token + " " + automata();
			break;
		    //sospecha de token mayor		
		    case '>':
			//Verifico que halla un caractermas en la cadena y si lo hay verifico si el siguiente es un igual
			if (longitud > 1 && cadena.charAt(1) == '=') {
			    //Si es un igual se consume dos letras, y retorna token_mayor_igual
			    valorToken = cadena.substring(0, 2);
			    cadena = cadena.substring(2);
			    token = creador.getToken(valorToken);
			    listaTokens.add(token);
			    cadenaRespuesta = token + " " + automata();
			} else {
			    //sino token mayor
			    valorToken = cadena.substring(0, 1);
			    cadena = cadena.substring(1);
			    token = creador.getToken(valorToken);
			    listaTokens.add(token);
			    cadenaRespuesta = token + " " + automata();
			}
			break;
		    case '<':
			//sospecha de token menor
			if (longitud > 1) {
			    //si hay un caracter mas verifico que tipo de caracter es
			    switch (cadena.charAt(1)) {
				case '=':
				    //retorna token menor igual consume dos letras
				    valorToken = cadena.substring(0, 2);
				    cadena = cadena.substring(2);
				    token = creador.getToken(valorToken);
				    listaTokens.add(token);
				    cadenaRespuesta = token + " " + automata();
				    break;
				case '>':
				    //retorna token distinto consume dos letras

				    valorToken = cadena.substring(0, 2);
				    cadena = cadena.substring(2);
				    token = creador.getToken(valorToken);
				    listaTokens.add(token);
				    cadenaRespuesta = token + " " + automata();
				    break;
				default:
				    //Si es otro caracter cosume una letra, (la leida) y retorna tokenMenor
				    valorToken = cadena.substring(0, 1);
				    cadena = cadena.substring(1);
				    token = creador.getToken(valorToken);
				    listaTokens.add(token);
				    cadenaRespuesta = token + " " + automata();
				    break;
			    }
			} else {
			    //si es el ultimo caracter retorna token menor

			    valorToken = cadena.substring(0, 1);
			    cadena = cadena.substring(1);
			    token = creador.getToken(valorToken);
			    listaTokens.add(token);
			    cadenaRespuesta = token + " " + automata();
			}
			break;
		    case ':':
			//Sospecha de asignacion
			if (longitud > 1 && cadena.charAt(1) == '=') {

			    valorToken = cadena.substring(0, 2);
			    cadena = cadena.substring(2);
			    token = creador.getToken(valorToken);
			    listaTokens.add(token);
			    cadenaRespuesta = token + " " + automata();
			} else {

			    valorToken = cadena.substring(0, 1);
			    cadena = cadena.substring(1);
			    token = creador.getToken(valorToken);
			    listaTokens.add(token);
			    cadenaRespuesta = token + " " + automata();
			}
			break;
		    default:
			valorToken = cadena.substring(0, 1);
			cadena = cadena.substring(1);
			token = creador.getToken(valorToken);
			listaTokens.add(token);
			cadenaRespuesta = token + " " + automata();
			//cadenaRespuesta = "Caracter_No_Valido = " + puntero + automata();
			break;
		}
	    }
	}
	return cadenaRespuesta;
    }

    public String consumirComentario() {
	//Te consume la cadena hasta e fin del comentario
	//Sino esta se consume toda la cadena y se llama al automata
	int posicion;
	String respuesta = "";
	if ((cadena.indexOf("}")) == -1) {
	    //Si el comentario no cierra se borra toda la cadena, y retorna mensaje de error
	    cadena = "";
	    try {
		while (!(cadena = lector.retornarLinea()).contains("}")) {

		    if (cadena.equalsIgnoreCase("Fin de archivo")) {

			respuesta = "Falta_cerrar_comentario ";
			return respuesta;
		    }
		    cadena = "";
		}
	    } catch (IOException ex) {
		return "Falta_cerrar_comentario ";
		//Logger.getLogger(AnalizadorLexicoPascal.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
	//el fin de comentario puede estar en otras lineas
	if ((posicion = cadena.indexOf("}")) != -1) {
	    cadena = cadena.substring(posicion + 1);
	}
	return respuesta;
    }

    public String sospechaNumero() {
	//Si se encontro un numero hay sospecha de numero
	//Se consumen todos los numeros, cuando lo encuentra retorna el token_numero_lexema
	int longitud, i = 1;
	char puntero;
	Token token;
	longitud = cadena.length();
	String cadenaConsumida = "" + String.valueOf(cadena.charAt(0));
	boolean tokenEncontrado = false;
	while (i < longitud && !tokenEncontrado) {
	    puntero = cadena.charAt(i);
	    if (!Character.isDigit(puntero)) {
		tokenEncontrado = true;
	    } else {
		cadenaConsumida = cadenaConsumida + puntero;
	    }
	    i++;
	}
	token = creador.getToken("Numero", cadenaConsumida);
	listaTokens.add(token);

	cadena = cadena.substring(cadenaConsumida.length(), cadena.length());
	return token.toString();
    }

    public String sospechaId() {
	//Si se encontro una letra hay sospecha de id
	int longitud, i = 1;
	char puntero;
	Token token;
	longitud = cadena.length();
	String cadenaConsumida = "" + String.valueOf(cadena.charAt(0));
	boolean tokenEncontrado = false;
	while (i < longitud && !tokenEncontrado) {
	    puntero = cadena.charAt(i);
	    if (!esLetra(puntero) && !Character.isDigit(puntero)) {
		tokenEncontrado = true;
	    } else {
		cadenaConsumida = cadenaConsumida + puntero;
	    }
	    i++;
	}
	//Se crea el token el objeto creador se encarga de saber si es palabra reservada o un id
	token = creador.getToken("id", cadenaConsumida);
	listaTokens.add(token);
	cadena = cadena.substring(cadenaConsumida.length(), cadena.length());
	return token.toString();
    }

    public static boolean esLetra(char puntero) {
	boolean respuesta = false;
	//Verifico si es una letra segun definido en nuestro alfabeto comparando con los valores de la tabla ascii
	if ((puntero >= 65 && puntero <= 90) || (puntero >= 97 && puntero <= 122) || puntero == 95) {
	    respuesta = true;
	}
	return respuesta;
    }

    public Token obtenerToken() throws NoSuchElementException {
	Token retorno;
	if (listaTokens.isEmpty()) {
	    obtenerLineaConToken();
	}
	//System.out.println();
	//System.out.println("En la lista queda " + listaTokens.toString());

	retorno = listaTokens.getFirst();
	listaTokens.remove();
	return retorno;
    }

    public String obtenerLista() {
	return listaTokens.toString();
    }

    public int obtenerLinea() {
	return lector.numeroLinea();
    }

    public void obtenerLineaConToken() {
	try {
	    cadena = lector.retornarLinea();
	} catch (IOException ex) {
	    Logger.getLogger(AnalizadorLexicoPascal.class.getName()).log(Level.SEVERE, null, ex);
	}
	if (cadena.equalsIgnoreCase("fin de archivo")) //finalizo la lectura del archivo
	{
	    System.out.println("Se terminó el archivo de entrada y falta cerrar el programa");
	    System.exit(0);
	} else {
	    automata();
	}

	if (listaTokens.isEmpty()) {
	    obtenerLineaConToken();
	}
    }

}
