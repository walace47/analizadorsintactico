package analisadorLexico;

import java.util.HashMap;

public class CreadorTokens {

    private HashMap<String, Token> mapa;

    public CreadorTokens() {
	mapa = new HashMap<>();
	mapa.put("<", new Token("Operador Logico", "<"));
	mapa.put("<=", new Token("Operador Logico", "<="));
	mapa.put(">", new Token("Operador Logico", ">"));
	mapa.put(">=", new Token("Operador Logico", ">="));
	mapa.put("<>", new Token("Operador Logico", "<>"));
	mapa.put("=", new Token("Operador Logico", "="));
	mapa.put("+", new Token("Operador Aritmetico", "+"));
	mapa.put("-", new Token("Operador Aritmetico", "-"));
	mapa.put("*", new Token("Operador Aritmetico", "*"));
	mapa.put("/", new Token("Operador Aritmetico", "/"));
	mapa.put(":=", new Token("Operador de Asignacion", ":="));
	mapa.put(".", new Token("Caracter de Puntuacion", "."));
	mapa.put(",", new Token("Caracter de Puntuacion", ","));
	mapa.put(";", new Token("Caracter de Puntuacion", ";"));
	mapa.put(":", new Token("Caracter de Puntuacion", ":"));
	mapa.put("(", new Token("Caracter de Puntuacion", "("));
	mapa.put(")", new Token("Caracter de Puntuacion", ")"));
	mapa.put("program", new Token("Palabra Reservada", "program"));
	mapa.put("procedure", new Token("Palabra Reservada", "procedure"));
	mapa.put("function", new Token("Palabra Reservada", "function"));
	mapa.put("boolean", new Token("Palabra Reservada", "boolean"));
	mapa.put("integer", new Token("Palabra Reservada", "integer"));
	mapa.put("var", new Token("Palabra Reservada", "var"));
	mapa.put("begin", new Token("Palabra Reservada", "begin"));
	mapa.put("end", new Token("Palabra Reservada", "end"));
	mapa.put("if", new Token("Palabra Reservada", "if"));
	mapa.put("then", new Token("Palabra Reservada", "then"));
	mapa.put("else", new Token("Palabra Reservada", "else"));
	mapa.put("while", new Token("Palabra Reservada", "while"));
	mapa.put("do", new Token("Palabra Reservada", "do"));
	mapa.put("and", new Token("Palabra Reservada", "and"));
	mapa.put("or", new Token("Palabra Reservada", "or"));
	mapa.put("true", new Token("Palabra Reservada", "true"));
	mapa.put("false", new Token("Palabra Reservada", "false"));
	mapa.put("not", new Token("Palabra Reservada", "not"));
    }

    public Token getToken(String valor) {
	Token token;
	token = mapa.get(valor);
	if (token == null) {
	    token = new Token("Caracter No valido", valor);
	}
	return token;
    }

    public Token getToken(String tipo, String valor) {
	//solo de debe mandar id y numeros
	Token token;
	if (tipo.equalsIgnoreCase("id")) {
	    token = mapa.get(valor.toLowerCase());
	    if (token == null) {
			token = new Token("Id", valor.toLowerCase());
	    }
	} else {
	    token = new Token("Numero", valor);
	}
	return token;
    }

}
