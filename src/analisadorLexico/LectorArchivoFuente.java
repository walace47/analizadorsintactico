package analisadorLexico;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class LectorArchivoFuente {

    int lineas;

    private FileReader file;
    private BufferedReader reader;

    public LectorArchivoFuente(String archivo) throws FileNotFoundException {
	String path = new File("").getAbsolutePath();
	file = new FileReader(path + "/" + archivo);
	reader = new BufferedReader(file);
	lineas = 0;
    }

    public String retornarLinea() throws IOException {
	String cadena, respuesta;

	if ((cadena = reader.readLine()) != null) {
	    respuesta = cadena + "\n";
	    lineas++;
	} else {
	    reader.close();
	    respuesta = "Fin de archivo";
	}
	return respuesta;
    }

    public int numeroLinea() {
	return this.lineas;
    }

}
