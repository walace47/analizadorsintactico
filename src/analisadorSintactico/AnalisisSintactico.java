package analisadorSintactico;

import analisadorLexico.AnalizadorLexicoPascal;
import analisadorLexico.CreadorTokens;
import analisadorLexico.Token;

public class AnalisisSintactico {

    Token preanalisis;
    CreadorTokens creador;
    AnalizadorLexicoPascal lexico;

    public AnalisisSintactico(String archivo) {
	lexico = new AnalizadorLexicoPascal(archivo);
	creador = new CreadorTokens();
	preanalisis = lexico.obtenerToken();
    }

    public void match(Token t) {
	//System.out.println("Se quiere consumir" + t + " \n y tiene " + preanalisis);
	if (preanalisis.equals(t)) {
	    try {
		preanalisis = lexico.obtenerToken();
	    } catch (Exception e) {
		System.err.println("No pudo terminar correctamente y se esperaba " + t);
		System.exit(0);
	    }
	} else {
	    error("Se esperaba " + t + " y se leyo " + preanalisis);
	}
    }

    private boolean igualValor(String valor) {
	return preanalisis.getValor().equals(valor);
    }

    private boolean igualTipo(String valor) {
	return preanalisis.getTipo().equals(valor);
    }

    public void error(String detalle) {
	System.out.println("Error: " + detalle + " en la linea " + lexico.obtenerLinea());
	//System.out.println(preanalisis.toString());
	System.exit(0);
    }

    public void program() {
	match(creador.getToken("program"));
	id();
	match(creador.getToken(";"));
	head();
	body();
	if (igualValor(".")) {
	    //finalizó el programa
	    System.out.println("Se termino la compilación correctamente");
	} else {
	    //se hace match para que tire el error
	    match(creador.getToken("."));
	}
    }

    public void head() {
	if (igualValor("procedure") || igualValor("function")
		|| igualValor("var")) {
	    switch (preanalisis.getValor()) {
		case ("procedure"):
		    procedure();
		    break;
		case ("function"):
		    function();
		    break;
		case ("var"):
		    varDeclare();
		    break;
	    }
	    head();
	}
    }

    private void procedure() {
	match(creador.getToken("procedure"));
	id();
	parameters();
	match(creador.getToken(";"));
	head();
	body();
	match(creador.getToken(";"));
    }

    private void function() {
	match(creador.getToken("function"));
	id();
	parameters();
	match(creador.getToken(":"));
	type();
	match(creador.getToken(";"));
	head();
	body();
	match(creador.getToken(";"));
    }

    private void parameters() {
	if (igualValor("(")) {
	    match(creador.getToken("("));
	    varType();
	    moreParameters();
	    match(creador.getToken(")"));
	}
    }

    private void moreParameters() {
	if (igualValor(";")) {
	    match(creador.getToken(";"));
	    varType();
	    moreParameters();
	}
    }

    private void varDeclare() {
	match(creador.getToken("var"));
	varType();
	match(creador.getToken(";"));
	moreVarType();
    }

    private void moreVarType() {
	if (igualTipo("Id")) {
	    // si encuentra un id es que hay mas declaraciones
	    varType();
	    match(creador.getToken(";"));
	    moreVarType();
	}
    }

    private void varType() {
	id();
	moreId();
	match(creador.getToken(":"));
	type();
    }

    private void moreId() {
	if (igualValor(",")) {
	    match(creador.getToken(","));
	    id();
	    moreId();
	}
    }

    private void type() {
	switch (preanalisis.getValor()) {
	    case ("boolean"):
		match(creador.getToken("boolean"));
		break;
	    case ("integer"):
		match(creador.getToken("integer"));
		break;
	    default:
		error("se esperaba boolean o integer y se leyo "+ preanalisis.getValor());
	}
    }

    public void body() {
	match(creador.getToken("begin"));
	sentence();
	match(creador.getToken("end"));
    }

    private void sentence() {
	if (igualValor("if") || igualValor("while") || igualValor("begin")
		|| igualTipo("Id")) {
	    sentenceAux();
	    sentenceAux2();
	}
    }

    private void sentenceAux() {
	if (igualTipo("Id")) {
	    id();
	    callOrAssig();
	} else {
	    switch (preanalisis.getValor()) {
		case ("if"):
		    ifControl();
		    break;
		case ("while"):
		    whileControl();
		    break;
		case ("begin"):
		    body();
		    break;
		default:
		    error("Se espera una instruccion while,begin, o if y se encontro " + preanalisis.getValor());
	    }
	}
    }

    private void sentenceAux2() {
	if (igualValor(";")) {
	    match(creador.getToken(";"));
	    sentence();
	}
    }

    private void callOrAssig() {
	switch (preanalisis.getValor()) {
	    case (":="):
		assig();
		break;
	    default: //callParameter puede ser lambda por lo que no se sabe que puede venir después
		callParameter();
	}
    }

    private void callParameter() {
	if (igualValor("(")) {
	    match(creador.getToken("("));
	    exp();
	    moreExpParameter();
	    match(creador.getToken(")"));
	}
    }

    private void moreExpParameter() {
	if (igualValor(",")) {
	    match(creador.getToken(","));
	    exp();
	    moreExpParameter();
	}
    }

    public void id() {
	if (igualTipo("Id")) {
	    try {
		preanalisis = lexico.obtenerToken();
	    } catch (Exception e) {
		System.err.println("No pudo terminar correctamente ");
		System.exit(0);
	    }
	} else {
	    error("identificador no valido");
	}
    }

    private void assig() {
	match(creador.getToken(":="));
	exp();
    }

    private void ifControl() {
	match(creador.getToken("if"));
	exp();
	match(creador.getToken("then"));
	sentenceAux();
	ifElse();
    }

    private void ifElse() {
	if (igualValor("else")) {
	    match(creador.getToken("else"));
	    sentenceAux();
	}
    }

    private void whileControl() {
	match(creador.getToken("while"));
	exp();
	match(creador.getToken("do"));
	sentenceAux();
    }

    private void value() {
	if (igualValor("false") || igualValor("true")) {
	    booleanControl();
	} else {
	    number();
	}
    }

    private void number() {
	if (igualTipo("Numero")) {
	    try {
		preanalisis = lexico.obtenerToken();
	    } catch (Exception e) {
		System.err.println("No pudo terminar correctamente");
		System.exit(0);
	    }
	} else {
	    error("numero no valido");
	}
    }

    private void exp() {
	exp1();
	expAux();
    }

    private void expAux() {
	if (igualValor("or")) {
	    match(creador.getToken("or"));
	    exp1();
	    expAux();
	}
    }

    private void exp1() {
	exp2();
	expAux1();
    }

    private void expAux1() {
	if (igualValor("and")) {
	    match(creador.getToken("and"));
	    exp2();
	    expAux1();
	}
    }

    private void exp2() {
	exp3();
	expAux2();
    }

    private void expAux2() {
	if (igualValor("=") || igualValor("<>") || igualValor(">")
		|| igualValor("<") || igualValor(">=") || igualValor("<=")) {
	    op3();
	    exp3();
	    expAux2();
	}
    }

    private void exp3() {
	exp4();
	expAux3();
    }

    private void expAux3() {
	if (igualValor("+") || igualValor("-")) {
	    op2();
	    exp4();
	    expAux3();
	}
    }

    private void exp4() {
	exp5();
	expAux4();
    }

    private void expAux4() {
	if (igualValor("*") || igualValor("/")) {
	    op1();
	    exp5();
	    expAux4();
	}

    }

    private void exp5() {
	if (igualValor("not")) {
	    match(creador.getToken("not"));
	    exp5();
	} else {
	    exp6();
	}
    }

    private void exp6() {
	if (igualValor("-")) {
	    match(creador.getToken("-"));
	    exp6();
	} else {
	    fact();
	}
    }

    private void fact() {
	if (igualTipo("Numero") || igualValor("false") || igualValor("true")) {
	    value();
	} else if (igualTipo("Id")) {
	    id();
	    callParameter();
	} else if (igualValor("(")) {
	    match(creador.getToken("("));
	    exp();
	    match(creador.getToken(")"));
	} else {
	    error("se espera una expresión");
	}
    }

    private void op3() {
	switch (preanalisis.getValor()) {
	    case ("="):
		match(creador.getToken("="));
		break;
	    case ("<>"):
		match(creador.getToken("<>"));
		break;
	    case (">"):
		match(creador.getToken(">"));
		break;
	    case ("<"):
		match(creador.getToken("<"));
		break;
	    case (">="):
		match(creador.getToken(">="));
		break;
	    case ("<="):
		match(creador.getToken("<="));
		break;
	    default:
		error("se espera un operador de comparacion");
	}
    }

    private void op2() {
	switch (preanalisis.getValor()) {
	    case ("+"):
		match(creador.getToken("+"));
		break;
	    case ("-"):
		match(creador.getToken("-"));
		break;
	    default:
		error("se espera un operador de adicion o sustraccion");
	}
    }

    private void op1() {
	switch (preanalisis.getValor()) {
	    case ("*"):
		match(creador.getToken("*"));
		break;
	    case ("/"):
		match(creador.getToken("/"));
		break;
	    default:
		error("se espera un operador de multiplicacion o division");
	}
    }

    private void booleanControl() {
	switch (preanalisis.getValor()) {
	    case ("true"):
		match(creador.getToken("true"));
		break;
	    case ("false"):
		match(creador.getToken("false"));
		break;
	    default:
		error("se espera un elemento booleano");
	}
    }

}
